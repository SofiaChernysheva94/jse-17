package ru.t1.chernysheva.tm.api.service;

import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.enumerated.Sort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById (String Id, Status status);

    Task changeTaskStatusByIndex (Integer index, Status status);

    List<Task> findAll(Sort sort);

}
