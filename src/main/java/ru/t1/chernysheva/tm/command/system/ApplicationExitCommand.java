package ru.t1.chernysheva.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    private final String NAME = "exit";

    private final String DESCRIPTION = "Close application.";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
