package ru.t1.chernysheva.tm.command.system;

import ru.t1.chernysheva.tm.api.model.ICommand;
import ru.t1.chernysheva.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private final String NAME = "help";

    private final String ARGUMENT = "-h";

    private final String DESCRIPTION = "Show command list.";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command: commands)  System.out.println(command);
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
