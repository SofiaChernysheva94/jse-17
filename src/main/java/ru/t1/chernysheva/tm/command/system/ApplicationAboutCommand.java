package ru.t1.chernysheva.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String NAME = "about";

    private final String ARGUMENT = "-a";

    private final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Sofia Chernysheva");
        System.out.println("E-mail: schernysheva@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
