package ru.t1.chernysheva.tm.command.task;

import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    private final String NAME = "task-show-by-id";

    private final String DESCRIPTION = "Show task by Id.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
