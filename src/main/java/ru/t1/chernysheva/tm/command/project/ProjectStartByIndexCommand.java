package ru.t1.chernysheva.tm.command.project;

import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand{

    private final String NAME = "project-start-by-index";

    private final String DESCRIPTION = "Start project by index.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
