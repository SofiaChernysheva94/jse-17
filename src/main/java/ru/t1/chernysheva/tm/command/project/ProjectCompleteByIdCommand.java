package ru.t1.chernysheva.tm.command.project;

import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-complete-by-id";

    private final String DESCRIPTION = "Complete project by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return NAME;
    }

    @Override
    public String getName() {
        return DESCRIPTION;
    }

}
