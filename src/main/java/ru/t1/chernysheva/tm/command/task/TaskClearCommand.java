package ru.t1.chernysheva.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    private final String NAME = "task-clear";

    private final String DESCRIPTION = "Delete all tasks.";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
